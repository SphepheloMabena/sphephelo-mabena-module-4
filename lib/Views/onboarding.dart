
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sphephelo_mabena_module_4/Views/Registration.dart';

import 'Login.dart';

class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(appBar: AppBar(
      title: Text("Onboarding"),
      backgroundColor: Colors.blueAccent,
    ),

      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 100),
            child: Center(
              child: SizedBox(
                height: 250,
                width: 280,
                child: Image(
                  image: AssetImage("images/welcomer.jpg")
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Center(
              child: TextButton(
                onPressed: (){
                  Navigator.push(context,MaterialPageRoute(
                      builder: (context){
                    return Login();
                  }));
                },
                child: Container(
                  color: Colors.blueAccent,
                  height: 50,
                  child: Center(child: Text("Login",
                  style: TextStyle(
                    color: Colors.black
                  ),)),
                  width: 200,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Center(
              child: TextButton(
                onPressed: (){
                  Navigator.push(context,MaterialPageRoute(
                    builder: (context){
                      return Registration();
                    }
                  ));
                },
                child: Container(
                  color: Colors.blueAccent,
                  height: 50,
                  child: Center(child: Text("Register",
                  style: TextStyle(
                    color: Colors.black
                  ),)),
                  width: 200,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
