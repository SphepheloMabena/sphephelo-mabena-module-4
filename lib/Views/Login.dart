import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sphephelo_mabena_module_4/Views/Dashboard.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
          backgroundColor: Colors.blueAccent,
        ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top:260),
              child: Center(
                child: SizedBox(
                  width: 300,
                  height: 50,
                  child: TextField(
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.brown,
                        fontSize: 15
                    ),
                    decoration: InputDecoration(
                        labelText: "Email",
                        icon: Icon(Icons.info),
                        fillColor: Colors.brown,
                        focusColor: Colors.brown,
                        border: UnderlineInputBorder(
                        )

                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top:40),
              child: Center(
                child: SizedBox(
                  width: 300,
                  height: 50,
                  child: TextField(
                    obscureText: true,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.brown,
                        fontSize: 15,


                    ),
                    decoration: InputDecoration(
                        labelText: "Password",
                        icon: Icon(Icons.lock),
                        fillColor: Colors.brown,
                        focusColor: Colors.brown,
                        border: UnderlineInputBorder(
                        )

                    ),
                  ),
                ),
              ),
            ),
            //add a button here
            Padding(
              padding: const EdgeInsets.only(top: 150),
              child: TextButton(
                onPressed: (){
                  Navigator.push(context,MaterialPageRoute(
                      builder: (context){
                    return Dashboard();
                  }));
                },
                child: Container(
                  color: Colors.blueAccent,
                  width: 200,
                  height: 40,
                  child: Center(child: Text("Login",
                  style: TextStyle(
                    color: Colors.white
                  ),)),
                ),
              ),
            )
          ],
        ),
      )
    );
  }
}
