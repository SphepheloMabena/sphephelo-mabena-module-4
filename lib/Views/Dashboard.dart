import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sphephelo_mabena_module_4/Views/GymPlans.dart';

import 'EditProfile.dart';

class Dashboard extends StatelessWidget {
  const Dashboard ({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
          backgroundColor: Colors.blueAccent,
        ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text("Welcome to the gym App",
                style: TextStyle(
                  fontSize: 17,
                  color: Colors.blueAccent
                ),),
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top:20),
                child: SizedBox(
                  width:300 ,
                  height: 200,
                  child: Image(
                    image: AssetImage("images/gym.jpg"),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 180),
              child: TextButton(
                onPressed: (){
                  Navigator.push(context,MaterialPageRoute(
                    builder: (context){
                      return GymPlans();
                    }
                  ));
                },
                child: Container(
                  color: Colors.blueAccent,
                  width: 200,
                  height: 40,
                  child: Center(child: Text("Our gym plans",
                    style: TextStyle(
                        color: Colors.white
                    ),)),
                ),
              ),
            ),

          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.push(context,MaterialPageRoute(builder: (context){
            return EditProfile();
          }));},
        tooltip: 'Your account',
        child: const Icon(Icons.account_circle_sharp),
      ),
    );
  }
}
