import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sphephelo_mabena_module_4/Models/ProfileInformation.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit profile"),
        backgroundColor: Colors.blueAccent,
      ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top:120),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextFormField(
                      initialValue: ProfileInformation.name,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.brown,
                          fontSize: 15
                      ),
                      decoration: InputDecoration(
                          labelText: "Name",
                          icon: Icon(Icons.lock_clock),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextFormField(
                      initialValue: ProfileInformation.surname,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.brown,
                          fontSize: 15
                      ),
                      decoration: InputDecoration(
                          labelText: "Surname",
                          icon: Icon(Icons.lock_clock),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40),
                child: Center(
                  child: SizedBox(
                    width: 300,
                    height: 50,
                    child: TextFormField(
                      initialValue: ProfileInformation.email,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.brown,
                      ),
                      decoration: InputDecoration(
                          labelText: "Email",
                          icon: Icon(Icons.lock_clock),
                          fillColor: Colors.brown,
                          focusColor: Colors.brown,
                          border: UnderlineInputBorder(
                          )

                      ),
                    ),
                  ),
                ),
              ),

              //add a button here
              Padding(
                padding: const EdgeInsets.only(top: 60),
                child: TextButton(
                  onPressed: (){},
                  child: Container(
                    color: Colors.blueAccent,
                    width: 200,
                    height: 40,
                    child: Center(child: Text("Save",
                      style: TextStyle(
                          color: Colors.white
                      ),)),
                  ),
                ),
              )
            ],
          ),
        )
    );
  }
}
