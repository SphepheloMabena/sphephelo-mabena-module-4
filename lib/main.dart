// @dart=2.9
import 'package:flutter/material.dart';

import 'package:sphephelo_mabena_module_4/Views/onboarding.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(MaterialApp(
home: SplashScreen(
      seconds: 4,
      navigateAfterSeconds:OnboardingScreen(),
      title: Text(
        'Sphephelo Mabena Module 4',
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            color: Colors.blueAccent),
      ),
      backgroundColor: Colors.white,
      styleTextUnderTheLoader: TextStyle(),
      loaderColor: Colors.blueAccent
  ))
  );
}
